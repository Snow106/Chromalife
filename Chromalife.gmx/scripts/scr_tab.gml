//pd for point direction - get the start location
pd = (point_direction(xstart, ystart, x, y))

//right
if ((pd > 315) || (pd < 45))
{
    with (obj_greyblot)
    {
        //move right
        x += xSpeed
        right = 1
        image_xscale = 1
    }
}
//up
if ((pd > 45) || (pd < 135))
{
    with (obj_greyblot)
    {
        //move up
       if (!place_free(x,y+1))
       {
            vspeed = jumpSpeed
       }
    }
}

//left
if ((pd > 135) || (pd < 225))
{
    with (obj_greyblot)
    {
        //move left
        x -= xSpeed
        right = -1
        image_xscale = -1
    }
}

//down
if ((pd > 225) || (pd < 315))
{
    with (obj_greyblot)
    {
        //move down
        obj_greyblot.statueDrop = true;
    }
}
else
{
    with(obj_greyblot)
    {
        obj_greyblot.statueDrop = false;
    }
}
