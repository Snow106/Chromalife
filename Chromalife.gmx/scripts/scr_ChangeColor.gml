//sets the active color to whatever color is clicked on. argument 0 is a color, such as blue, yellow, etc.
if (visible = true)
{
    if (argument[0] == global.activeColor)
    {
        global.activeColor = "gray"
    }
    else if (visible = true)
    {
        global.activeColor = argument[0]
        if (global.activeColor = "yellow")
        {
            part_emitter_region(Sname,emitter1,obj_greyblot.x,obj_greyblot.x,obj_greyblot.y-92,obj_greyblot.y-92,0,0);
            part_emitter_burst(Sname,emitter1,particle1,20);
        }
    }
    
        if global.soundEnabled = true
        {
            if !audio_is_playing(shudder1||shudder2||shudder3||shudder4||shudder5)
            audio_play_sound((choose(shudder1, shudder2, shudder3, shudder4, shudder5)), 4,false)
        }
}
