//greyblot step event we had before//

hsp = dir * greySpeed;
vsp += grav;
closeToFloor = place_meeting( x, y+vspeed, obj_floor ) && (vspeed >=0)
closeToCeiling = place_meeting( x, y+vspeed, obj_floor ) && (vspeed <0)

//////////DOOR TEMP

if !instance_exists(obj_blueStatue)
    {
        if instance_exists(obj_blueDoor)
            with (obj_blueDoor)
                {
                    instance_destroy()
                }
    }



//HORIZONTAL COLLISION/////

if (place_meeting(x+hsp, y, obj_wall))
    {   
    onGround = true;
        while(!place_meeting(x+sign(hsp), y, obj_wall))
            {
                x += sign(hsp)
            }
        hsp = 0
    }
    x += hsp

//vertical collision


if (place_meeting(x,y+vsp, obj_floor))
    {
    onGround = true;
        while(!place_meeting(x,y+sign(vsp), obj_floor))
            {
                y += sign(vsp)
            }
        vsp = 0
    }

    y += vsp
   

///MOVEMENT///


if (moveLeft)
            {
            image_xscale = -1;
            dir = -1
            hsp = dir * greySpeed
            }
else if (moveRight)
            {
            image_xscale = 1;
            dir = 1
            hsp = dir * greySpeed
            }
else
            {
            dir = 0;
            hsp = 0;
            }
            
///JUMPING///

if (closeToFloor) 
{
    onGround = true;
    gravity = 0;
    repeat(vspeed)
    {
        if (!place_meeting( x, y+1, obj_floor )) && (!onRamp)
        {
            y+=1
        }
    }
    vspeed = 0;
}
else
{
    onGround = false;
}

if (closeToCeiling)
{
    canJump = false;
    vspeed = 0;
}
    
//on the ground, so jumping is allowed
if (onGround) 
{
    canJump = true;  
    gravity = 0; 
    //Jumping
    if (moveJump)
    {
        motion_add(90,jumpSpeed)
        onGround = false;
    }
} 
else 
{
    gravity = grav;
}

