if speed = 0
{
    sprite_index = spr_ravenIdle
} else
{
    sprite_index = spr_crow
}

if instance_exists(obj_target)
{
    if distance_to_object(obj_greyblot) <200
    {
    
    dir = point_direction(x,y,obj_target.x,obj_target.y)
    spd = 10
    //CAW CAW MOTHER BUCKETS
        if global.soundEnabled = true && (cawed_once = false)
            {
                audio_play_sound(crow_caw,1,false)
                cawed_once = true
            }
    motion_set(dir,spd) 
    }
    if (place_meeting(x,y,obj_target))
    {
        speed=0
            with(obj_target)
            {
                instance_destroy();
            }
    }

}

if instance_exists(obj_target2)// || (place_meeting(x,y,obj_target))
{
    if distance_to_object(obj_greyblot) <100
    {
    
    dir = point_direction(x,y,obj_target2.x,obj_target2.y)
    spd = 10
        //CAW CAW MOTHER BUCKETS
        if global.soundEnabled = true && (cawed_twice = false)
            {
                audio_play_sound(crow_caw,1,false)
                cawed_twice = true
            }
    
    motion_set(dir,spd) 
    }
    if (place_meeting(x,y,obj_target2))
    {
        speed=0
    }
}

