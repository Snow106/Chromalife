//if the player collides with fall object - if the player falls in a hole.
if global.soundEnabled = true
{
    audio_play_sound((choose(death1,death2,death3,death4,death5)),1,false)
}
obj_greyblot.x = greyblotRespawnX;
obj_greyblot.y = greyblotRespawnY
