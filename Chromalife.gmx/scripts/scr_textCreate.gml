
if room = haida1_01
{
    message[0] =
    "These Lands are not so different from your own, though the hills are
    much younger and the trees far older than you may be used to."
}

if room = haida1_02
{
    message[0] =
    "This land could feel comfortable -
    were it not for total darkness."
}
if room = haida1_03
{
    message[0] =
    "Raven, as old as time itself was tired from eternally
    bumping into things in the dark."
}

if room = haida1_04
{
    message[0] =
    "Raven decided with all his slyness it was time
    to do something about this darkness."
}

if room = haida1_05
{
    message[0] =
    "Having discovered the keeper of all the light in the universe
    - an old man living with his daughter - the trickster Raven
    came up with a plan that requires your help!"
}

if room = haida1_06
{
    message[0] =
    "Raven waited for the old man’s daughter to fill her water basket
    in the river before turning himself into a hemlock needle."
}

if room = haida1_07
{
    message[0] = "Drinking from the basket, the girl swallowed the needle."
}

if room = haida1_08
{
    message[0] =
    "The raven slipped into her belly, transformed into a tiny human
    - often called a baby -
    and slept within her, growing for a very long time."
}

if room = haida1_09
{
    message[0] =
    "Though strange in appearance, Raven in his infant form
    was loved by his grandfather."
}

if room = haida1_10
{
    message[0] =
    "Protective of his treasure, grandfather threatened dire punishment
    if ever Raven-child touched the light."
}

if room = haida1_11
{
    message[0] =
    "Raven begged, and begged and finally to cease his yammering the old man opened the box,
    threw all the light in the universe to raven."
}

if room = roomHut
{
    message[0] =
    "Raven transformed, caught the light in his beak and escaped from the hut through the smoke hole.
    Raven, in all his slyness, deceived the old man for the betterment of all life on earth,for the
    needs of the many outweigh those of the few. And that is how all light came into the universe."
}


message_current=0; //0 is the first number in our array, and the message we are currently at

message_end=0; //3 is the last number in our array

message_draw=""; //this is what we 'write' out. It's blank right now

increase=1; //the speed at which new characters are added

characters=0; //how many characters have already been drawn

hold=0; //if we hold 'Z', the text will render faster

 
message_length=string_length(message[message_current]); //get the number of characters in the first message


