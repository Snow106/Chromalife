if (showInv)
{
    var x1,x2,y1,y2;
    x1 = view_xview[0];
    x2 = x1 + view_wview[0];
    y1 = view_yview[0];
    y2 = y1 + 48;

    draw_set_color(c_black);
    draw_set_alpha(0.8);
    draw_rectangle(x1,y1,x2,y2,0);
    draw_set_alpha(0.5);
    
    for (i = 0; i < maxItems; i += 1)
    {
        var ix = x1+24+(i * 40);
        var iy = y2-24;

        draw_sprite(spr_border,0,ix,iy)
        button[i].x = ix;
        button[i].y = iy;
    }
}
