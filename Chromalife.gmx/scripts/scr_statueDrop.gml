//Statue Dropping temp key spacebar
statueDrop = keyboard_check_pressed(ord("F"));

//When key is pressed - drop a statue
//Grey
if (statueDrop && statueColour = "gray")
{
    instance_create(obj_greyblot.x, obj_greyblot.y, obj_grayStatue);
}

//Blue
if (statueDrop && statueColour = "blue")
{
    instance_create(obj_greyblot.x, obj_greyblot.y, obj_blueStatue);
}

//Red
if (statueDrop && statueColour = "red")
{
    instance_create(obj_greyblot.x, obj_greyblot.y, obj_redStatue);
}

//Yellow
if (statueDrop && statueColour = "yellow")
{
    instance_create(obj_greyblot.x, obj_greyblot.y, obj_yellowStatue);
}



