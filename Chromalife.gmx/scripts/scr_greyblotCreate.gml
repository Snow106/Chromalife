//variables
inSlope = false;
image_speed= 1;
xSpeed = 15
jumpSpeed = -16
gravityAmount = 0.7

//Create the player with all variables
greyblotSpawnX = obj_greyblot.x;
greyblotSpawnY = obj_greyblot.y;

//Respawn location
greyblotRespawnX = obj_greyblot.x;
greyblotRespawnY = obj_greyblot.y - 500;

//Artifact Checks
canArti = true;
arti1Text = false;
arti1=false;

//Is greyblot moveable
moveable = true;

//touch buttons for controls
buttonRight = false;
buttonLeft = false;
buttonJump = false;
global.interact = false;


//touchButtons (to check if they're being executed)
touchLeft = false;
touchRight = false;
touchJump = false;
touchInteract = false;
jumpDevice = 5; // 5 is not a possible device, which is why they're set to this.
rightDevice = 5;
leftDevice = 5;
interactDevice = 5;
jumpButtonOn = 1;
touchLeftAndRight = false;

// AUDIO STUFF
hummed = true
//WHOA
if instance_exists(obj_audio_overlord)
{
    if global.soundEnabled = true
    {
        audio_play_sound((choose(whoa1,whoa2,whoa3,whoa4)),1,false)
    }
}

