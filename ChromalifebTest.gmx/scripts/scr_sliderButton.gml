if(mouse_check_button(mb_any))
{
    if(distance_to_point(mouse_x, mouse_y) < 1)
    {
        switch(buttonLevel)
        {
            case 1: global.volumeLevel = 1; break;
            case 2: global.volumeLevel = 2; break;
            case 3: global.volumeLevel = 3; break;
            case 4: global.volumeLevel = 4; break;
            case 5: global.volumeLevel = 5; break;
        }
    }
}
if (device_mouse_check_button( 0, mb_any))
{
    if(distance_to_point(device_mouse_x(0),device_mouse_y(0)) < 1)
    {
        switch(buttonLevel)
        {
            case 1: global.volumeLevel = 1; break;
            case 2: global.volumeLevel = 2; break;
            case 3: global.volumeLevel = 3; break;
            case 4: global.volumeLevel = 4; break;
            case 5: global.volumeLevel = 5; break;
        }
    }
}
if (device_mouse_check_button( 1, mb_any))
{
    if(distance_to_point(device_mouse_x(1), device_mouse_y(1)) < 1)
    {
        switch(buttonLevel)
        {
            case 1: global.volumeLevel = 1; break;
            case 2: global.volumeLevel = 2; break;
            case 3: global.volumeLevel = 3; break;
            case 4: global.volumeLevel = 4; break;
            case 5: global.volumeLevel = 5; break;
        }
    }
}

if (volumeLevel >= buttonLevel)
{
    image_index = 0;
}
else
{
    image_index = 1;
}

audio_master_gain(volumeLevel/5);

