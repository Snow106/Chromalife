if ((room != mainMenu) && (room != gameStart))
{
    if !(instance_exists (obj_jumpButton)) //checks to see if the buttons have been drawn or not. since all buttons are there at once (or not at all), only this is enough)
    {
        switch (global.controlScheme) //check to see current scheme
        {
            case 1:
                instance_create(0, room_height, obj_leftButton);
                instance_create(160, room_height, obj_rightButton);
                instance_create(640, room_height, obj_interactButton);
                instance_create(800, room_height, obj_jumpButton);
                instance_create(896, 8, obj_pause);
                break;
            case 2:
                instance_create(0, 384, obj_leftButton2);
                instance_create(640, 384, obj_rightButton2);
                instance_create(640, 160, obj_interactButton2);
                instance_create(0, 160, obj_jumpButton2);
                instance_create(896, 8, obj_pause);
                break;
        }
        
    }
}
else
{
    with(obj_HUDButtonParent)
    {
        instance_destroy();
    }
}

with (obj_HUDButtonParent)
{
    x = startX + view_xview
    y = startY + view_yview
}

