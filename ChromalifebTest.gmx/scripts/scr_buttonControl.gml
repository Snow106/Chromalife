//jump
if(jumpButtonOn = 1)
{
    if ( (device_mouse_check_button(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_jumpButton)) || (device_mouse_check_button(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_jumpButton)) || (device_mouse_check_button(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_jumpButton)) ||  (device_mouse_check_button(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_jumpButton)) || (device_mouse_check_button(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_jumpButton)) )
    {
        jumpButtonOn = 0;
        touchJump = true;
    }
}
else
{
    touchJump = false; 
    //jumpButtonOn = 1;
}

if ( (device_mouse_check_button_released(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_jumpButton)) || (device_mouse_check_button_released(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_jumpButton)) || (device_mouse_check_button_released(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_jumpButton)) ||  (device_mouse_check_button_released(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_jumpButton)) || (device_mouse_check_button_released(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_jumpButton)) )
{
    jumpButtonOn =1;
}


//interact     
if ( (device_mouse_check_button(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_interactButton)) || (device_mouse_check_button(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_interactButton)) || (device_mouse_check_button(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_interactButton)) ||  (device_mouse_check_button(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_interactButton)) || (device_mouse_check_button(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_interactButton)) )
{
    touchInteract = true;
    global.interact = true;
}
else
{
  touchInteract = false;
  global.interact = false;  
}

//statue
if ( (device_mouse_check_button_pressed(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_greyblot)) || (device_mouse_check_button_pressed(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_greyblot)) || (device_mouse_check_button_pressed(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_greyblot)) ||  (device_mouse_check_button_pressed(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_greyblot)) || (device_mouse_check_button_pressed(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_greyblot)) )
{
    //destroy all existing statues
    if (instance_exists(obj_grayStatue))
    {
        with (obj_grayStatue)
        {
            instance_destroy()
        }
    }
    if (instance_exists(obj_yellowStatue))
    {
        with (obj_yellowStatue)
        {
            instance_destroy()
        }
    }
    if (instance_exists(obj_blueStatue))
    {
        with (obj_blueStatue)
        {
            instance_destroy()
        }
    }
    if (instance_exists(obj_redStatue))
    {
        with (obj_redStatue)
        {
            instance_destroy()
        }
    }
    //CREATE statue!
    if (global.activeColor = "gray")
    {
        //instance_create(x,y,obj_grayStatue)
    }
    if (global.activeColor = "yellow")
    {
        //instance_create(x,y,obj_yellowStatue)
    }
    if (global.activeColor = "red")
    {
        //instance_create(x,y,obj_redStatue)
    }
    if (global.activeColor = "blue")
    {
        //instance_create(x,y,obj_blueStatue)
    }
}

//right 
if ( (device_mouse_check_button(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_rightButton)) || (device_mouse_check_button(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_rightButton)) || (device_mouse_check_button(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_rightButton)) ||  (device_mouse_check_button(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_rightButton)) || (device_mouse_check_button(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_rightButton)) )
{
    touchRight = true;  
}
else
{
    touchRight = false;
}

//left 
if ( (device_mouse_check_button(0, mb_any) && position_meeting(device_mouse_x(0), device_mouse_y(0), obj_leftButton)) || (device_mouse_check_button(1, mb_any) && position_meeting(device_mouse_x(1), device_mouse_y(1), obj_leftButton)) || (device_mouse_check_button(2, mb_any) && position_meeting(device_mouse_x(2), device_mouse_y(2), obj_leftButton)) ||  (device_mouse_check_button(3, mb_any) && position_meeting(device_mouse_x(3), device_mouse_y(3), obj_leftButton)) || (device_mouse_check_button(4, mb_any) && position_meeting(device_mouse_x(4), device_mouse_y(4), obj_leftButton)) )
{
    touchLeft = true;      
}    
else
{
    touchLeft = false;
}

