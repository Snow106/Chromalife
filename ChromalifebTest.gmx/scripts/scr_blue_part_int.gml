particle3 = part_type_create();
part_type_shape(particle3,pt_shape_disk);
part_type_size(particle3,0.08,0.13,0,0);
part_type_scale(particle3,1,1);
part_type_colour1(particle3,$EE861C);
part_type_alpha2(particle3,1,0.10);
part_type_speed(particle3,3.50,4,-0.10,0);
part_type_direction(particle3,0,359,3,0);
part_type_orientation(particle3,0,0,0,0,1);
part_type_blend(particle3,1);
part_type_life(particle3,10,30);

