x=view_xview[0]
y=view_yview[0]

xt=view_xview[0]+120

//draw_self()
breaks = string_count('#',message_draw)
font_size = font_get_size(TextFont)
    maxlength = y+840;
    text_width = string_width_ext(message_draw,font_size+(font_size), maxlength);
    text_height = string_height_ext(message_draw,font_size+(font_size), maxlength);
//TEXT SHADOW
draw_set_colour(c_dkgray)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt+3 , y+3, message_draw + "");
draw_set_alpha(1)

//TEXT BOX
draw_set_colour(c_dkgray)
draw_set_alpha(0.7)
draw_set_halign(fa_left);
draw_rectangle(x+120-16, y, x+840+16, y+60+16,false)
//draw_rectangle(x, y,x+string_width(message_draw)+16,y+ text_height+16,false)
//draw_rectangle(x, y, string_width(message_draw)+16, font_get_size(TextFont)* 4,false)
draw_set_alpha(1)

draw_set_colour(c_white)
//draw_set_alpha(0.5)
draw_set_halign(fa_left);
draw_rectangle(x+120-16, y, x+840+16, y+60+16,true)
//draw_rectangle(x, y,x+string_width(message_draw)+16,y+ text_height+16,true)
//draw_rectangle(x, y, string_width(message_draw)+16, font_get_size(TextFont)* 4,false)
draw_set_alpha(1)

//TEXT OUTLINES
draw_set_colour(c_black)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt+1 , y, message_draw + "");
draw_set_alpha(1)

draw_set_colour(c_black)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt-1 , y, message_draw + "");
draw_set_alpha(1)

draw_set_colour(c_black)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt , y+1, message_draw + "");
draw_set_alpha(1)

draw_set_colour(c_black)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt , y-1, message_draw + "");
draw_set_alpha(1)

//MESSAGE
draw_set_colour($2adeff)
draw_set_font(TextFont)
draw_set_halign(fa_left);
draw_text( xt , y, message_draw + "");
draw_set_alpha(1)

