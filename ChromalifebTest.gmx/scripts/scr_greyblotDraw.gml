        draw_set_color(c_black);
        draw_set_font(clueFont);
    

    if instance_exists(obj_clueBox)
    {
        draw_set_color(c_black);
        draw_set_font(clueFont);
        
        /////level 1-03 adze clue/////
        if (room= haida1_03)
        {
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+24, "Adzes were used for ")
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+44, "chopping, smoothing and")
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+64, "carving wood.")
        }
        
        /////level 1-06 fish clue/////
        if (room= haida1_06)
        {
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+24, "These boxes were")
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+44, "used for cooking salmon.")
        }
        
        /////level 1-07 oar clue/////
        if (room= haida1_07)
        {
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+24, "Oars like this were ")
            draw_text(obj_clueBox.x+7.5, obj_clueBox.y+44, "used for steering and ")
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+64, "propelling a canoe.")
        }
        
        /////level 1-07 oar clue/////
        if (room= haida1_09)
        {
            draw_text(obj_clueBox.x +7.5, obj_clueBox.y+24, "Clam baskets, made from ")
            draw_text(obj_clueBox.x+7.5, obj_clueBox.y+44, "wicker can hold many clams.")
        }
    }    

draw_self();
