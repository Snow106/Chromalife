globalvar soundEnabled;
soundEnabled = true
globalvar narrationEnabled;
narrationEnabled = true


if global.soundEnabled = true
{
    //CITY AMBIENCE

    if room = greyblotHouse or room = greyWorld or room = greyWorld2
    {

        if !audio_is_playing(city_ambiance)
        {
        audio_stop_sound(forest_ambiance)
        audio_play_sound(city_ambiance,3,true)
        }
    }

    //FOREST AMBIANCE
    if  room =haida1_01 or room = haida1_02 or room = haida1_03 or
    room = haida1_04 or room = haida1_05 or room = haida1_06 or
    room = haida1_07 or room = haida1_08 or room = haida1_09 or
    room = haida1_10 or room = haida1_11 or room = roomHut
    {

        if !audio_is_playing(forest_ambiance)
        {
        audio_stop_sound(city_ambiance)
        audio_play_sound(forest_ambiance,3,true)
        }

    }

}

    //NARRATION
if global.narrationEnabled = true
{
    if room = haida1_01
    {
        audio_play_sound(haida_narration1,1,false)
    
    }
    if room = haida1_02
    {
        audio_stop_sound(haida_narration1)
        audio_play_sound(haida_narration2,1,false)
    
    }
    if room = haida1_03
    {
        audio_stop_sound(haida_narration2)
        audio_play_sound(haida_narration3,1,false)
    
    }
    if room = haida1_04
    {
        audio_stop_sound(haida_narration3)
        audio_play_sound(haida_narration4,1,false)
    
    }
    if room = haida1_05
    {
        audio_stop_sound(haida_narration4)
        audio_play_sound(haida_narration5,1,false)
    
    }
    if room = haida1_06
    {
        audio_stop_sound(haida_narration5)
        audio_play_sound(haida_narration6,1,false)
    
    }
    if room = haida1_07
    {
        audio_stop_sound(haida_narration6)
        audio_play_sound(haida_narration7,1,false)
    
    }
    if room = haida1_08
    {
        audio_stop_sound(haida_narration7)
        audio_play_sound(haida_narration8,1,false)
    
    }
    if room = haida1_09
    {
        audio_stop_sound(haida_narration8)
        audio_play_sound(haida_narration9,1,false)
    
    }
    if room = haida1_10
    {
        audio_stop_sound(haida_narration9)
        audio_play_sound(haida_narration10,1,false)
    
    }
    if room = haida1_11
    {
        audio_stop_sound(haida_narration10)
        audio_play_sound(haida_narration11,1,false)
    
    }
    if room = roomHut
    {
        audio_stop_sound(haida_narration11)
        audio_play_sound(haida_narration12,1,false)
    
    }
}
