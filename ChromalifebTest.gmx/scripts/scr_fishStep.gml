image_speed = 0

if instance_exists(obj_pineNeedle)
{
    if y < 800 { y = 800}
    
    //TARGET FOUND
    if (distance_to_object(obj_pineNeedle)) <= 1000
    {
        image_index = 1
            if (obj_pineNeedle.x > x)
            {
                motion_set(0,1)
            }
            if (obj_pineNeedle.x < x)
            {
                motion_set(180,1)
            }
    }
    
    //TARGET LOST
    else if (distance_to_object(obj_pineNeedle.x)) >= 500
    {
        image_index = 2
    }
    
    if (place_meeting(x,y,obj_pineNeedle))
    {
        room_restart();
    }

}

//Repelled by yellow statues and blocks
if distance_to_object(obj_yellowStatue) <=15
{
        if (obj_yellowStatue.x > x)
        {
            motion_set(180,1)
        }
        if (obj_yellowStatue.x < x)
        {
            motion_set(0,1)
        }
}


