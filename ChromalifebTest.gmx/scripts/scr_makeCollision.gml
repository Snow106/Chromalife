if visible
    {
    
    }


/*

s = "x"  // s = weather the x or y of greyblot is being moved
n = 1               // n = the sign (weather we are substracting or adding)
i = 1               // i = the value that's being added to grayblot's x or y
j = 0               // j = the "step" counter. There are 8 steps: one for every position we try to move greyblot to (left, right, up or down) and one for every time we move him back;
h = 0               // h = determines weather s is x or y. (changes every two steps)

if (global.activeColor = argument0)
{

    solid = true;
    while(place_meeting(x, y, obj_greyblot))
    {
        if (j=0 || j=3 || j=5 || j=6)
        {  
            n = -1;
        }
        else if (j=1 || j=2 || j=4 || j=7)
        {
            n = 1;
        }
        
        if (s="x")
        { 
            obj_greyblot.x = obj_greyblot.x+(n*i)
        }
        else if (s="y")
        {
            obj_greyblot.y = obj_greyblot.y+(n*i)
        }
        
        j ++;
        h ++;
        if(j =8)
        {  
            i++;
            j=0;
        }
        if(h = 2)
        {
            if (s = "x")
            {
                s = "y"
            }
            else if (s = "y")
            {
                s = "x"
            }
            h = 0;
        }
            
        
    }
    if(place_meeting(x, y, obj_greyblot))
     {
        obj_greyblot.y -= 1
    }
}

else
{
    solid = false;
}

