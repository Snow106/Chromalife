//animations go here
if (global.activeColor = "gray")
{
    if (place_meeting(x,y+2,obj_slopeStart))
    {
        sprite_index = spr_greyblot_tumble
        image_speed = .5
    }
    else if (vspeed !=0) //jump
    {
        sprite_index = spr_greyblot_jump
        image_speed = .4;
    }          
    else if ((input_left || touchLeft ) && touchLeftAndRight = false)
    {
        if touchInteract = true
        {
            sprite_index = spr_greyblot_push     
        }
        else
        {
            sprite_index = spr_greyblot_walk
        }
        image_xscale = -1
        image_speed = .5;
        right = -1
        
    }
    
    else if ((input_right || touchRight) && touchLeftAndRight = false)
    {
        if touchInteract = true
        {
            sprite_index = spr_greyblot_push     
        }
        else
        {
            sprite_index = spr_greyblot_walk
        }
            image_xscale = 1
            image_speed = .5
            right = -1
    }
    else if touchInteract = true
    {
        sprite_index = spr_greyblot_push
        image_speed = 0
    }
            
    else
    {
    sprite_index = spr_greyblot
    image_speed = .5
    }
    statueColour = "gray"
}
if (global.activeColor = "red")
{
    image_index = 1
    statueColour = "red"
}
if (global.activeColor = "blue")
{
    image_index = 2
    statueColour = "blue"
}
if (global.activeColor = "yellow")
{
    if (place_meeting(x,y+2,obj_slopeStart))
    {
        sprite_index = spr_greyblot_tumble_yellow
        image_speed = .5
    }
    else if (vspeed !=0)
    {
        sprite_index = spr_greyblot_jump_yellow
        image_speed = .4;
    }          
    else if input_left || touchLeft 
    {
        if touchInteract = true
        {
            sprite_index = spr_greyblot_push_yellow     
        }
        else
        {
            sprite_index = spr_greyblot_walk_yellow
        }
        image_xscale = -1
        image_speed = .5;
        right = -1
        
    }
    
    else if input_light || touchRight
    {
        if touchInteract = true
        {
            sprite_index = spr_greyblot_push_yellow     
        }
        else
        {
            sprite_index = spr_greyblot_walk_yellow
        }
            image_xscale = 1
            image_speed = .5
            right = -1
    }
    else if touchInteract = true
    {
        sprite_index = spr_greyblot_push_yellow
        image_speed = 0
    }
            
    else
    {
    sprite_index = spr_greyblot_yellow
    image_speed = .5
    }
    statueColour = "yellow" 
}
