//  NEW COLLISION STUFF //
//Physics objects must call the parent object's create event
//to initialize physics variables.  Here we are calling it
//BEFORE we initialize the player's stats because obj_moveable
//also defines S_SLOPE_SLOW for itself and we want to
//overwrite the value of it with one specifically for obj_player
event_inherited();

//Here some player stats are defined (so they can be easily
//tweaked if needed).
S_GRAVITY       = 1.0;      //Accel. due to gravity (pixels/step*step)
S_RUN_ACCEL     = 1.2;      //Accel. from running on ground (pixels/step*step) //how fast
S_RUN_FRIC      = 18;      //Friction on the ground (pixels/step*step)
S_AIR_ACCEL     = 1.4;      //Accel. from running in the air (pixels/step*step)
S_AIR_FRIC      = 1.6;      //Friction in the air (pixels/step*step)
S_JUMP_SPEED    = -13;        //how far
S_DJUMP_SPEED   = -12.0;       //Double jump speed
S_MAX_H         = 9.6;      //Max horizontal speed
S_MAX_V         = 14.5;      //Max vertical speed  //floating
S_SLOPE_SLOW    = 0.8;      //Decceleration while climbing slopes

//Animation
image_speed = 0.5;
step_cooldown = 15;
active = true;
facing = 1;

//No idea what this stuff does again? YOLO
play_onground = false;
dead = false;


/////////////////////////////////////
//variables
inSlope = false;
//image_speed= 1;
xSpeed = 15
//jumpSpeed = -16
gravityAmount = 0.7

//Create the player with all variables
greyblotSpawnX = obj_greyblot.x;
greyblotSpawnY = obj_greyblot.y;

//Respawn location
greyblotRespawnX = obj_greyblot.x;
greyblotRespawnY = obj_greyblot.y;

//Artifact Checks
canArti = true;
arti1Text = false;
arti1=false;

//Is greyblot moveable
moveable = true;

//touch buttons for controls
buttonRight = false;
buttonLeft = false;
buttonJump = false;
global.interact = false;


//touchButtons (to check if they're being executed)
touchLeft = false;
touchRight = false;
touchJump = false;
touchInteract = false;
jumpDevice = 5; // 5 is not a possible device, which is why they're set to this.
rightDevice = 5;
leftDevice = 5;
interactDevice = 5;
jumpButtonOn = 1;
touchLeftAndRight = false;

// AUDIO STUFF
hummed = true
//WHOA
if instance_exists(obj_audio_overlord)
{
    if global.soundEnabled = true
    {
        audio_play_sound((choose(whoa1,whoa2,whoa3,whoa4)),1,false)
    }
}

i = 0;
canDustJump = true;
canDust = true;
canWalkDust = true;

canSplat = true;

if room=haida1_01
    {
    sprite_index=spr_fall;
    }
    
    
boxAbove = true;

//Dust
dustNum = 0

//View
old_view_xview=view_xview;
old_view_yview=view_yview;
