lastX = obj_greyblot.x;

if (moveable)
{

/*      CONTROLS      */

var input_right = keyboard_check( vk_right ) || keyboard_check( ord("D"));
var input_left = keyboard_check( vk_left ) || keyboard_check( ord("A"));
var input_up = keyboard_check_pressed( vk_up ) || keyboard_check_pressed( ord("W")) || keyboard_check_pressed( vk_space );

/*      PLAYER INPUT EVAL       */

var accel, fric;
if (on_ground) {
    accel = S_RUN_ACCEL;
    fric = S_RUN_FRIC;
} else {
    accel = S_AIR_ACCEL;
    fric = S_AIR_FRIC;
}

if active {
if place_meeting(x,y+5,obj_slopeR) || place_meeting(x,y+5,obj_slopeL)
    {
    on_ground = false;
    }


if input_right|| touchRight || place_meeting(x,y+5,obj_slopeR){
    //Running right
    //First add friction if currently running left
    if (hspd < 0)
        hspd = approach( hspd, 0, fric );        
    hspd = approach( hspd, S_MAX_H, accel ); 
    //image & animation
    facing = 1;
    state = 'walk';

} else if input_left|| touchLeft || place_meeting(x,y+5,obj_slopeL){
    //Running left
    //First add friction if currently running right
    if (hspd > 0)
        hspd = approach( hspd, 0, fric ); 
    hspd = approach( hspd, -S_MAX_H, accel ); 
    //image & animation
    facing = -1;
    state = 'walk';
    
} else {
    //Stopping
    hspd = approach( hspd, 0, fric );
    state = 'stand';
}


if (on_ground) {
    //canDustJump = false;
    candj = true;
    jumpcharge = false;
    //Jumping
    if input_up || touchJump{
       vspd = S_JUMP_SPEED;
       if state != 'doublejump' && state != 'jump'{
          //Fix sometimes glitchy audio
          if !audio_is_playing(hey1){
             //scr_sound(sfx_jump);
          }
       }
    }
    //Charging the double jump
    alarm[0] = 5;        
} else {
    //Gravity
    vspd = approach( vspd, S_MAX_V, S_GRAVITY );
    state = 'jump';
    //canDustJump = true;
    //Double jumping
    /*if input_up {
        if candj && jumpcharge = true{
           candj = false;
           vspd = S_DJUMP_SPEED;
           state = 'doublejump';
           //scr_sound(sfx_dj);*/
        }
    }
}

/*      AMIMATION AND IMAGE      */

image_xscale = facing
if state == 'walk' {
    image_speed = 0.1;
    sprite_index = spr_greyblot_walk;
    if (floor(image_index) == 1 or floor(image_index) == 5) and step_cooldown<0{
        step_cooldown = 1;
    } else step_cooldown-=15;
}else if state == 'stand' {
    step_cooldown = 0;
    image_speed = 0.1;
    sprite_index = spr_greyblot;
    image_index = 0;
}if state == 'jump' {
    step_cooldown = 0;
    image_speed = 0.1;
    sprite_index = spr_greyblot_jump;
    image_index = 1;
}



        /*
if (moveJump || touchJump)
{
    //alarm[6] = 2 //not sure if we really need this... probably don't but it makes our code fancy so fuck it.
    if (!place_free( x,y+1))
        {
   //     with instance_create(x,y,Dust)
   //     image_xscale = other.right
        vspeed = jumpSpeed

        }
        
}

if (place_free(x,y+1+vspeed))
{
    gravity=gravityAmount
}
else
{
    gravity=0
}
        */


//artifact check for clues
if (arti1) && (canArti)
{
    instance_create(x-150,y-300,obj_clueBox)
    canArti = false;
    //draw_text(view_xview[0] + 25, view_yview[0] +25, "canArti=false");
    atri1Text = true;
    //draw_text(view_xview[0] + 25, view_yview[0] +50, "arti1Text=true");
}

//if the player collides with fall object - if the player falls in a hole.
if place_meeting(x,y,obj_fallVolume)
    {    
    if global.soundEnabled = true
    {
        audio_play_sound((choose(death1,death2,death3,death4,death5)),1,false)
    }
    obj_greyblot.x = greyblotRespawnX;
    obj_greyblot.y = greyblotRespawnY
    }

//Statue Dropping temp key f
//statueDrop = keyboard_check_pressed(ord("F"));

//When key is pressed - drop a statue
//Grey
//if (statueDrop && statueColour = "gray")
{
    //instance_create(obj_greyblot.x, obj_greyblot.y, obj_grayStatue);
}

//Blue
//if (statueDrop && statueColour = "blue")
{
    //instance_create(obj_greyblot.x, obj_greyblot.y, obj_blueStatue);
}

//Red
//if (statueDrop && statueColour = "red")
{
    //instance_create(obj_greyblot.x, obj_greyblot.y, obj_redStatue);
}

//Yellow
//if (statueDrop && statueColour = "yellow")
{
    //instance_create(obj_greyblot.x, obj_greyblot.y, obj_yellowStatue);
}



 //MAKE SURE STEP SHIT IS INSIDE THIS BRACKET. ;)

//animations go here
if (global.activeColor = "gray")
{

    if sprite_index = spr_greyblot_tumble
        {
        image_speed=.5

        }
             /*else if audio_is_playing(dirt_tumble)
            {
                audio_stop_sound(dirt_tumble)
            }*/

    if (place_meeting(x,y+5,obj_slopeR)) || (place_meeting(x,y+5,obj_slopeL))
    {
        if sprite_index !=spr_greyblot_tumble
            {
            sprite_index = spr_greyblot_tumble
            image_speed = .5
            }
        image_speed = .5
    }
    else if (vspeed !=0) //jump
    {
        sprite_index = spr_greyblot_jump
        image_speed = .4;
    }          
    else if ((input_left || touchLeft ) && touchLeftAndRight = false)
    {
        if (touchInteract = true)
        {
            if (instance_exists(obj_box) )
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_push
                    }
                else
                    {
                    sprite_index = spr_greyblot_pull
                    }
                }    
        }
        else
        {
            sprite_index = spr_greyblot_walk
        }
        image_xscale = -1
        image_speed = .5;
        right = -1
        
    }
    
    else if ((input_right || touchRight) && touchLeftAndRight = false)
    {
        if touchInteract = true
        {
            if instance_exists(obj_box)
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_pull
                    }
                else
                    {
                    sprite_index = spr_greyblot_push
                    }
                }      
        }
        else
        {
            sprite_index = spr_greyblot_walk
        }
            image_xscale = 1
            image_speed = .5
            right = -1
    }
    else if touchInteract = true
    {
        sprite_index = spr_greyblot_push
        image_speed = 0
    }
            
    else
    {
    sprite_index = spr_greyblot
    image_speed = .5
    }
    statueColour = "gray"
}

if (global.activeColor = "yellow")
{
    if (place_meeting(x,y+5,obj_slopeR)) || (place_meeting(x,y+5,obj_slopeL))
    {
        sprite_index = spr_greyblot_tumble_yellow
        image_speed = .5
    }
    else if (vspeed !=0)
    {
        sprite_index = spr_greyblot_jump_yellow
        image_speed = .4;
    }          
    else if input_left || touchLeft 
    {
        if touchInteract = true
        {
            if instance_exists(obj_box)
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_push_yellow
                    }
                else
                    {
                    sprite_index = spr_greyblot_pull_yellow
                    }
                }    
        }
        else
        {
            sprite_index = spr_greyblot_walk_yellow
        }
        image_xscale = -1
        image_speed = .5;
        right = -1
        
    }
    
    else if input_right || touchRight
    {
        if touchInteract = true
        {
        if instance_exists(obj_box)
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_pull_yellow
                    }
                else
                    {
                    sprite_index = spr_greyblot_push_yellow
                    }
                }      
        }
        else
        {
            sprite_index = spr_greyblot_walk_yellow
        }
            image_xscale = 1
            image_speed = .5
            right = -1
    }
    else if touchInteract = true
    {
        sprite_index = spr_greyblot_push_yellow
        image_speed = 0
    }
            
    else
    {
    sprite_index = spr_greyblot_yellow
    image_speed = .5
    }
    statueColour = "yellow" 
}

if (global.activeColor = "blue")
{
    if (place_meeting(x,y+5,obj_slopeR)) || (place_meeting(x,y+5,obj_slopeL))
    {
        sprite_index = spr_greyblot_tumble_yellow
        image_speed = .5
    }
    else if (vspeed !=0)
    {
        sprite_index = spr_greyblot_jump_yellow
        image_speed = .4;
    }          
    else if input_left || touchLeft 
    {
        if touchInteract = true
        {
            if instance_exists(obj_box)
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_push_blue
                    }
                else
                    {
                    sprite_index = spr_greyblot_pull_blue
                    }
                }    
        }
        else
        {
            sprite_index = spr_greyblot_walk_blue
        }
        image_xscale = -1
        image_speed = .5;
        right = -1
        
    }
    
    else if input_right || touchRight
    {
        if touchInteract = true
        {
        if instance_exists(obj_box)
                {
                if obj_box.x < x
                    {
                    sprite_index = spr_greyblot_pull_blue
                    }
                else
                    {
                    sprite_index = spr_greyblot_push_blue
                    }
                }      
        }
        else
        {
            sprite_index = spr_greyblot_walk_blue
        }
            image_xscale = 1
            image_speed = .5
            right = -1
    }
    else if touchInteract = true
    {
        sprite_index = spr_greyblot_push_blue
        image_speed = 0
    }
            
    else
    {
    sprite_index = spr_greyblot_blue
    image_speed = .5
    }
    statueColour = "blue" 
}

//  TREE PARALLAXING    //
if ((input_left || touchLeft)and place_free(x-2,y))
{

        if view_xview[0]<old_view_xview//x !=lastX && xSpeed > 0
        {
            ///BGTREE PARALLAX
            if instance_exists(obj_bgTreeB)
                with (obj_bgTreeB)
                    {
                        x+=0.5
                    }
             if instance_exists(obj_hutBG)
                with (obj_hutBG)
                    {
                        x+=0.5
                    }
            if instance_exists(obj_bgTreeM)
                with (obj_bgTreeM)
                    {
                        x+=1
                    }
            if instance_exists(obj_bgTreeF)
                with (obj_bgTreeF)
                    {
                        x+=1.5
                    }
            
            if instance_exists(obj_city1b2)
                with (obj_city1b2)
                {
                    x+=1
                }
            if instance_exists(obj_city1b3)
                with (obj_city1b3)
                {
                    x+=1.5
                }
                
            if instance_exists(obj_city2b2)
                with (obj_city2b2)
                {
                    x+=1
                }
            if instance_exists(obj_city2b3)
                with (obj_city2b3)
                {
                    x+=1.5
                }
                
            if instance_exists(obj_bgMF)
                with (obj_bgMF)
                {
                    x+=1.5
                }
                
            if instance_exists(obj_bgMM)
                with (obj_bgMM)
                {
                    x+=1
                }
            if instance_exists(obj_bgMB)
                with (obj_bgMB)
                {
                    x+=1.5
                }
        }
                  
}

if ((input_right || touchRight)and place_free(x+2,y))
{

    if view_xview[0]>old_view_xview//x !=lastX && xSpeed > 0
    {
        ///BGTREE PARALLAX
        if instance_exists(obj_bgTreeB)
            with (obj_bgTreeB)
                {
                    x-=0.5
                }
        if instance_exists(obj_hutBG)
                with (obj_hutBG)
                    {
                        x-=0.5
                    }
        if instance_exists(obj_bgTreeM)
            with (obj_bgTreeM)
                {
                    x-=1
                }
        if instance_exists(obj_bgTreeF)
            with (obj_bgTreeF)
                {
                    x-=1.5
                }
                
        if instance_exists(obj_city1b2)
            with (obj_city1b2)
                {
                    x-=1
                }
        if instance_exists(obj_city1b3)
            with (obj_city1b3)
                {
                    x-=1.5
                }

            if instance_exists(obj_city2b2)
                with (obj_city2b2)
                {
                    x-=1
                }
            if instance_exists(obj_city2b3)
                with (obj_city2b3)
                {
                    x-=1.5
                }
                
            if instance_exists(obj_bgMF)
                with (obj_bgMF)
                {
                    x-=1.5
                }
                
            if instance_exists(obj_bgMM)
                with (obj_bgMM)
                {
                    x-=1
                }
            if instance_exists(obj_bgMB)
                with (obj_bgMB)
                {
                    x-=1.5
                }
    }
    
}

/////TEST SETTING VARIABLES/////
//every time the player receives a new colour, the variable grows

if keyboard_check_pressed(vk_add)
    {
    global.totalColor += 1;
    }
    
//if past variable range, back to 0 to reset    
if global.totalColor >= 4
    {
    global.totalColor = 0;
    }
    
if keyboard_check_pressed(ord('N'))
{
    room_goto_next();
}

if keyboard_check_pressed(vk_escape)
{
    game_end();
}

if place_meeting(x,y+5,obj_floor) && canDust
    {
    dust1 = instance_create(x,y,obj_dustJump);
    //canDustJump=false
    if canDustJump
        {
        while (i <= dust1.dustNum)
            {
            instance_create(x+random_range(-30,30),y-random_range(-5,10),obj_dustJump);
            i+=1;
            }
            if i >= dust1.dustNum
                {
                i=0
                canDust = false;
                dust1 = 0;
                }
        }
    }
    
    if !place_meeting(x,y+5,obj_floor)
        {
        canDustJump = true;
        canDust = true
        }

if place_meeting(x,y+5,obj_floor) && canWalkDust && hspd !=0
    {
    instance_create(x,y, obj_dustWalk)
    canWalkDust = false;
    alarm[11]=5;
    }

if sprite_index=spr_fall
    {
    moveable=false;
    if image_index>image_number-1
        {
        image_speed=0;
        
        if place_meeting(x,y,obj_floor)
            {
            sprite_index=spr_splat;
            image_speed=1;
            }
        
        }
    }

if sprite_index=spr_splat
    {
    if image_index>image_number-1
        {
        moveable=true;
        sprite_index=obj_greyblot;
        }
    }
    
// View
old_view_xview=view_xview;
old_view_yview=view_yview;



////////////////////////////////////////////

if instance_exists(obj_slopeR)
          {
            if distance_to_object(obj_slopeR) < 5 || distance_to_object(obj_slopeL) > 5
           {
            
           }
        else
            {
           x -= 10
           }
          }







