particle2 = part_type_create();
part_type_shape(particle2,pt_shape_disk);
part_type_size(particle2,0.08,0.13,0,0);
part_type_scale(particle2,1,1);
part_type_color2(particle2,8454143,65535);
part_type_alpha2(particle2,1,0.10);
part_type_speed(particle2,3.50,4,-0.10,0);
part_type_direction(particle2,0,359,3,0);
part_type_orientation(particle2,0,0,0,0,1);
part_type_blend(particle2,1);
part_type_life(particle2,10,30);

