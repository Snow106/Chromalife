if global.activeColor = "gray"
    {
    with(obj_yellowVis)
        {
        image_index = 1
        }
    with(obj_blueVis)
        {
        image_index = 1
        }
    with(obj_redVis)
        {
        image_index = 1
        }
    }
if global.activeColor = "yellow"
    {
    with(obj_yellowVis)
        {
        image_index = 0
        }
    with(obj_blueVis)
        {
        image_index = 1
        }
    with(obj_redVis)
        {
        image_index = 1
        }
    }
    
if global.activeColor = "blue"
    {
    with(obj_yellowVis)
        {
        image_index = 1
        }
    with(obj_blueVis)
        {
        image_index = 0
        }
    with(obj_redVis)
        {
        image_index = 1
        }
    }

/*if global.totalColor <= 0
    {
    with(obj_blueVis)
        {
        image_index = 1;
        }
    with(obj_redVis)
        {
        image_index = 1;
        }
    with(obj_yellowVis)
        {
        image_index = 1;
        }
    with(obj_yellowBut)
    {
        visible = false;
    }
    }
if global.totalColor >= 1
    {
    with(obj_yellowVis)
        {
        image_index = 0;
        }
        with(obj_yellowBut)
    {
        visible = true;
    }
    with(obj_blueVis)
        {
        image_index = 1;
        }
    with(obj_redVis)
        {
        image_index = 1;
        }
    }
    
if global.totalColor >= 2
    {
    with(obj_yellowVis)
        {
        image_index = 0;
        }
    with(obj_yellowBut)
    {
        visible = true;
    }
    with(obj_blueVis)
        {
        image_index = 0;
        }
    with(obj_redVis)
        {
        image_index = 1;
        }
    }
    
if global.totalColor >= 3
    {
    with(obj_yellowVis)
        {
        image_index = 0;
        }
    with(obj_yellowBut)
    {
        visible = true;
    }
    with(obj_blueVis)
        {
        image_index = 0;
        }
    with(obj_redVis)
        {
        image_index = 0;
        }
    }
*/
    if instance_exists(obj_drum)
    {
    if totalColor >= 1
        {
        obj_drum.visible = true;
        }
    else
        {
        obj_drum.visible = false;
        }
    }
