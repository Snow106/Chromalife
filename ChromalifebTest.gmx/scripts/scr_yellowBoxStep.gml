    dir = 1;
    hsp = dir * obj_greyblot.xSpeed;

if distance_to_object(obj_wall) < 5 || distance_to_object(obj_slopeStart) < 5
   {    
        //dont move on top
        if place_meeting(x, y-30, obj_greyblot)
        {
            dir = 0;
            hsp = 0;
        }
        //////////////////////////////////////////////////////////////////////////////////
        //if greyblot is to my left, move right
        //push to right
        
        else if (distance_to_object(obj_greyblot) < 30) && global.interact
        {
            hsp = dir * obj_greyblot.xSpeed;
            x += obj_greyblot.xSpeed;
        }
        //pull to left
        else if (distance_to_object(obj_greyblot) < 50) && global.interact
        {
            hsp = dir * obj_greyblot.xSpeed;
            x -= obj_greyblot.xSpeed;
        }
        ////////////////////////////////////////////////////////////////////////////////
        // if greyblot is to my right, move left
        else if (distance_to_object(obj_greyblot) < 30) && global.interact
        {
            hsp = dir * obj_greyblot.xSpeed;
            x -= obj_greyblot.xSpeed;
        }
        
        else if (distance_to_object(obj_greyblot) < 50) && global.interact
        {
            hsp = dir * obj_greyblot.xSpeed;
            x += obj_greyblot.xSpeed;
        }

        else
        {
            dir = 0;
            hsp = 0;
        }
        
        if place_meeting(x,y,obj_jumpthru)
        {
            hsp=0
            vsp=0
            dir=0
            gravity=false
            
        }
        else
            {
                if (place_free(x,y+1+vspeed))
                {
                    with(self)
                    {
                    gravity=grav
                    }
                }
                else
                {
                    with(self)
                    {
                    gravity=0
                    }
                }
            }
        
        if place_meeting(x,y,obj_slopeStart)
            {
            move_towards_point(obj_slopeEnd.x,obj_slopeEnd.y, 10);
            }
        if place_meeting(x,y, obj_slopeEnd)
            {
            speed = 0;
            }
}
else
    {
        //y-=5;
    }
    
/////water/////
if instance_exists(obj_waterTarget)
{    
    if place_meeting(x,y, obj_waterTarget)
    {
        speed = 0;
        with (obj_waterTarget)
            {
            instance_destroy();
            instance_create(x,y+64,obj_jumpthru);
            }
    }
    if place_meeting(x,y, obj_water)
        {
            image_speed=0.2;
        if instance_exists(obj_waterTarget)
            {
            move_towards_point(obj_waterTarget.x,obj_waterTarget.y, 5);
            vsp=0;
            }
        }
}
else
{
hsp = 0;
vsp = 0;
grav = 0;
}

/////animation/////
if place_meeting(x,y,obj_slopeStart)
    {
        image_speed = 1;
    }
else
    {
        if global.interact = true && distance_to_object(obj_greyblot) < 33 && obj_greyblot.xSpeed > 0
        {
            image_speed = 1;
        }
        else
        {
            image_speed = 0;
        }
    }

/////gravity/////

if (place_free(x, y+1+vspeed))
{
    gravity = 0.7;
}
else
{
    gravity = 0;
}

